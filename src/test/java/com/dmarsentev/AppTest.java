package com.dmarsentev;

import static org.junit.Assert.assertTrue;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        List<StationTask.Station> STATION_LIST = Arrays.asList(
                new StationTask.Station("МОСКВА"),
                new StationTask.Station("МОЖГА"),
                new StationTask.Station("МОЗДОК"),
                new StationTask.Station("САНКТ-ПЕТЕРБУРГ"),
                new StationTask.Station("САМАРА"));

        StationTask task = new StationTask(STATION_LIST );
        List<StationTask.Station> result = (List<StationTask.Station>) task.getStationsByTwoFirstLetters("мо");

        List<StationTask.Station> expected = Arrays.asList(
                new StationTask.Station("МОСКВА"),
                new StationTask.Station("МОЖГА"),
                new StationTask.Station("МОЗДОК")
                );


        assertTrue(CollectionUtils.isEqualCollection(result,expected));


    }
}
