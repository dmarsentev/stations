package com.dmarsentev;

import java.util.*;

/*
   Задача 2.
   Есть список станций, который мы загружаем при инициализации класса.
   У каждой станции есть уникальное название.
   Когда пользователь вводит первые 2 буквы названия станции, мы должны вывести ему список подходящих станций.
   Для этого нужно реализовать метод getStationsByTwoFirstLetters.
   Таких станций и запросов очень много, поэтому вариант с перебором всех станций по каждому запросу не подходит.
   Нужно найти оптимальное решение по производительности.

*/

public class StationTask {

    private Map<String,List<Station>> prefixToStation;

    private static List<Station> STATION_LIST = Arrays.asList(
            new Station("МОСКВА"),
            new Station("МОЖГА"),
            new Station("МОЗДОК"),
            new Station("САНКТ-ПЕТЕРБУРГ"),
            new Station("САМАРА"));

    public static void main(String[] args) {
        StationTask task = new StationTask(STATION_LIST);
        System.out.println(task.getStationsByTwoFirstLetters("МО"));
        System.out.println(task.getStationsByTwoFirstLetters("СА"));
        System.out.println(task.getStationsByTwoFirstLetters("ФР"));
    }

    StationTask(List<Station> stationList) {
         prefixToStation = new HashMap<>();

        for (Station station: stationList){
            String key = station.getName().toLowerCase().substring(0,2);
            if(prefixToStation.containsKey(key)){
                prefixToStation.get(key).add(station);
            }else {
                List<Station> stations = new ArrayList<>();
                stations.add(station);
                prefixToStation.put(key,stations);
            }
        }


    }

    Collection<Station> getStationsByTwoFirstLetters(String prefix) {

        if(prefixToStation.containsKey(prefix.toLowerCase())){
            return prefixToStation.get(prefix.toLowerCase());
        }else{
            return new ArrayList<Station>();
        }

    }

    static class Station {

        private String name;

        Station(String name) {
            this.name = name;
        }

        String getName() {
            return name;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Station)) return false;
            Station station = (Station) o;
            return Objects.equals(getName().toLowerCase(), station.getName().toLowerCase());
        }

        @Override
        public int hashCode() {
            return Objects.hash(getName().toLowerCase());
        }

        @Override
        public String toString() {
            return "Station{" +
                    "name='" + name + '\'' +
                    '}';
        }
    }
}
